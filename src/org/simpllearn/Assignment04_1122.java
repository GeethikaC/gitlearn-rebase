package org.simpllearn;

public class Assignment04_1122 {
	
	public static void main(String[] args) {
		String a = "CORNER HOUSE";
		String b = "2019-11-29 12:53 AM";
	    float c = 10.870f;
		float d = 2.009f;
		double e = 22.71d;
		
		System.out.println("+-----------------------+");
		System.out.println("|                       |");
		System.out.println("|     "+a+"      |");
		System.out.println("|                       |");
		System.out.println("| "+b+"   |");
		System.out.println("|                       |");
		System.out.println("| Gallons:        "+c+" |");
		System.out.println("| Price/gallon: $ "+d+" |");
		System.out.println("|                       |");
		System.out.println("| Fuel Total:  $ "+e+"  |");
		System.out.println("|                       |");
		System.out.println("+-----------------------+");
		
	}

}

package org.simpllearn;

import java.util.Scanner;

public class Assignment01_1205 {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.print("number of the day:");
		String choice = s.next();
		switch(choice) {
		case "0":
		case "7":
			System.out.println("Today is a Saturday!");
			break;
	 
		case "1":
			System.out.println("Today is a Sunday!");
			break;
		
		case "2":
			System.out.println("Today is a Monday!");
			break;
		
		case "3":
			System.out.println("Today is a Tuesday!");
			break;
		
		case "4":
			System.out.println("Today is a Wednesday!");
			break;
		
		case "5":
			System.out.println("Today is a Thursday!");
			break;
			
		case "6":
			System.out.println("Today is a Friday!");
			break;
		}
		
	}
}

package org.simpllearn;

public class Assignment05_1122 {
	
	public static void main(String[] args) {
		int x = 10;
		float y = 2.34f;
		double z = 56.98d;
		
		//addition
		int a = (int) (x + y +z) ;
		System.out.println("Sum of 3 variables x,y&z is "+a);
				
		//subtraction
		 double s = z-x+y;
		 System.out.println("Difference of 3 variables x,y&z is "+s);
		 
		//multiplication
		long m = (long) (x*y*z);
		System.out.println("Product of 3 variables x,y&z is "+m);
		
		//division
		float d = (float) (z*y/x);
		System.out.println("Result of division of 3 variables x,y&z is "+d);
		
	}

}

package org.simpllearn;

import java.util.Scanner;

public class Assignment01_1129 {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Hey, What's your name?");
		String name= s.next();
		System.out.println("\n"+"ok,"+name+", how old are you?");
		int age= s.nextInt();
		if(age<25) {
			if(age<18) {
				if(age<16) {
					System.out.println("You can't drive, "+name);
				}
				System.out.println("You can't vote, "+name);
			}
			System.out.println("You can't rent a car, "+name);
		}
		else {
			System.out.println("You can do anything that's legal, "+name);
	}

}
}

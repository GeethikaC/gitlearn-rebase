package org.simpllearn;

import java.util.Scanner;

public class Assignment02_1129 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Weekday number?");
		int wdn= s.nextInt();
		
		System.out.println("Weekday 1: Sunday"+"\n"+"Weekday 2: Monday"+"\n"+"Weekday 3: Tuesday"+"\n"+
				"Weekday 4: Wednesday"+"\n"+"Weekday 5: Thursday"+"\n"+"Weekday 6: Friday"+"\n"+
				"Weekday 7: Saturday"+"\n"+"Weekday 0: Saturday"+"\n");
		
		System.out.println("Weekday "+wdn+":");
		
		if (wdn < 8) {
			if (wdn==0 || wdn==7) {
				System.out.println("Today is a Saturday!");
			}
			else if (wdn==1) {
				System.out.println("Today is a Sunday!");
			}
			else if (wdn==2) {
				System.out.println("Today is a Monday!");
			}
			else if (wdn==3) {
				System.out.println("Today is a Tuesday!");
			}
			else if (wdn==4) {
				System.out.println("Today is a Wednesday!");
			}
			else if (wdn==5) {
				System.out.println("Today is a Thursday!");
			}
			else{
				System.out.println("Today is a Friday!");
			}
		}
		else {
			System.out.println("error");
		}
		
		}
	}


package org.simpllearn.ab;

import org.simpllearn.ab.A; 

public class B extends A {
	
	public static void main(String[] args) {

		A example = new A();
		example.protectedmethod();
		example.publicmethod();
		example.defaultmethod();
		
	}
}

package org.simpllearn;

import java.util.Scanner;

public class Assignment04_1205 {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.print("choice is:");
		String choice = s.next();
		switch(choice) {
		case "List":
			System.out.println("S.No. : 832786");
			System.out.println("Name  : Geethika");
			System.out.println("Age   : 25");
			break;
			
		case "Table" :
			System.out.println("+---------+----------+");
			System.out.println("+ S.No.   +   832786 +");
			System.out.println("+ Name    + Geethika +");
			System.out.println("+ Age     +       25 +");
			System.out.println("+---------+----------+");
		}
		
	}

}

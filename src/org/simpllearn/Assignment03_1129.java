package org.simpllearn;

import java.util.Scanner;

public class Assignment03_1129 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Hey, What's your name? (Sorry, I keep Forgetting.)");
		String name= s.next();
		System.out.println("\n"+"ok,"+name+", how old are you?");
		int age= s.nextInt();
		System.out.println("\n");
		
		if(age<25) {
			if(age<18) {
				if(age< 16) {
					System.out.println("You can't drive, "+name);
				}else {
					System.out.println("You can drive but not vote, "+name);
				}
			}else {
				System.out.println("You can vote but not rent a car, "+name);
			}
		}
		else {
			System.out.println("You can do pretty much anything, "+name);}
	}
}

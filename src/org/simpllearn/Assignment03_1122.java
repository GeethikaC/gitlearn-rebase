package org.simpllearn;

import java.util.Scanner;

public class Assignment03_1122 {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Please enter the following details so I can sell it for profit");
		System.out.print("first name:");
		String firstname= s.next();
		System.out.print("last name:");
		String lastname= s.next();
		System.out.print("Grade (9-12):");
		int grade= s.nextInt();
		System.out.print("Student ID:");
		long StudentID= s.nextLong();
		System.out.print("Login:");
		String Login= s.next();
		//long LoginID= s.nextLong();
		System.out.print("GPA(0.0-4.0):");
		float GPA= s.nextFloat();
		
		System.out.println("\n"+"Your information:"+"\n"+"\t"+"Login:"+Login+"\n"+"\t"+"ID: "+StudentID+"\n"+"\t"+"Name:"+lastname+
				","+firstname+"\n"+"\t"+"GPA: "+GPA+"\n"+"\t"+"Grade: "+grade);
		}
}
